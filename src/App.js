import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

let classNames = require('classnames');

const deck = [];
const house = [];
const player = [];

initilizeDeck();

/*
  ---   BLACKJACK   ---
  Pays 3 to 2
  Dealer must draw to 16 and stand on all 17's
  Insurance pays 2 to 1

  ---  GAME PHASES  ---
  0. start (start, options)
  1. bet (input, confirm)
  2. decision (stand, hit, (split, double, insure))
  3. result: win, lose, bust (play again)

  ---    RESULTS    ---
  1. natural blackjack: house pays player 1.5 times the BET
  2.
*/

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      deck: deck,
      house: house,
      player: player,
      funds: 100000,
      bet: 0,
      phase: 0,
    };
  }
  handleChange = (e) => {
    this.setState({
      bet: e.target.value
    });
  }
  handleBet = (e) => {
    if (this.state.bet === 0) {
      e.preventDefault();
    } else if (this.state.bet > 0){
      e.preventDefault();
      this.setState({
        funds: this.state.funds - this.state.bet
      });
      this.setPhase(2);
      this.deal();
      this.calculate(this.state.player);
    }
  }
  deal () {
    for (let i = 0; i < 2; i++) {
      this.setState({
        player: player.push(deck.pop()),
        house: house.push(deck.pop())
      });
    }
  }
  hit () {
    this.setState({
      player: player.push(deck.pop())
    });
    this.calculate(player);
  }
  stand () {
    this.compDecision();
  }
  calculate (hand) {
    let sum = 0
    let sum11 = 0
    for (let i = 0; i < hand.length; i++) {
      sum += hand[i].value;
      sum11 += hand[i].value;
      if (hand[i].value === 1) {
        sum11 += hand[i].value
      }
    }
    if (sum <= 21) {
      return sum;
    }
    if (sum11 > 21) {
      return sum;
    }
    if (sum11 === 21) {
      return sum11
    }
  }
  compDecision () {
    const playerSum = this.calculate(player);
    const houseSum = this.calculate(house);

    if (houseSum <= 16) {
      this.setState({
        house: house.push(deck.pop())
      });
    } else if (houseSum >= 17 || houseSum <= 21) {
      this.result(playerSum, houseSum);
    } else if (houseSum > 21) {
      this.result(playerSum, houseSum);
    }
  }
  result (playerSum, houseSum) {
    if (playerSum > houseSum && playerSum <= 21) {
      console.log('player wins');
      this.payout();
    } else {
      console.log('house wins');
    }
    this.setPhase(3);
  }
  payout () {
    this.setState({
      funds: 1.5 * this.state.bet + this.state.funds
    });
  }
  restart () {
    while (player.length > 0) {
      this.setState({
        player: deck.push(player.pop()),
        bet: 0
      });
    }
    while (house.length > 0) {
      this.setState({
        house: deck.push(house.pop())
      });
    }
    this.setPhase(1);
    shuffle(deck);
  }
  setPhase (x) {
    this.setState({
      phase: x
    });
  }
  render() {
    const phase = this.state.phase;
    let options;

    switch (phase) {
      // start game
      case 0: options =
        <div>
          <button onClick={() => this.setPhase(1)}>START</button>;
        </div>;
        break
      // betting phase
      case 1: options =
        <div>
          <form onSubmit={this.handleBet}>
            <input type="number" min="0" step="10" onChange={this.handleChange} />
            <button>BET</button>
          </form>
        </div>;
        break
      // decision phase
      case 2: options =
        <div>
          <button onClick={() => this.stand()}>STAND</button>
          <button onClick={() => this.hit(this.state.player)}>HIT</button>
        </div>;
        break
      // result phase
      case 3: options =
        <div>
          <button onClick={() => this.restart()}>RESTART</button>
        </div>;
        break
    }

    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Blackjack Simulator</h1>
        </header>
        <div className="options">
          {options}
        </div>
        <div className="coin">
          <p>FUNDS: {this.state.funds} BET: {this.state.bet}</p>
        </div>
        <GameTable
          deckHand={this.state.deck}
          playerHand={this.state.player}
          houseHand={this.state.house}
          funds={this.state.funds}>
        </GameTable>
      </div>
    );
  }
}
class GameTable extends Component {
  render () {
    const deckView = getCards(this.props.deckHand, true, false);
    return (
      <div className="gametable">
        <div className="deckview">{deckView}</div>
        <div className="houseview">
          <p>HOUSE</p>
          <Hand className="house" cards={house}></Hand>
        </div>
        <div className="playerview">
          <p>PLAYER</p>
          <Hand cards={player}></Hand>
        </div>
      </div>
    );
  }
}
class Hand extends Component {
  render () {
    return this.props.cards.map((card) => {
      return (
        <Card
          card = {card.card}
          isRed = {card.isRed}
          display = {card.display}
          suit = {card.suit}
          key = {card.id}>
        </Card>
      );
    });
  }
}
class Card extends Component {
  render () {
    let cardClasses = classNames({
      'card': true,
      'compress': this.props.compress,
      'hidden': this.props.hidden,
      'red': this.props.isRed,
      'black': !this.props.isRed,
      'key': this.props.id
    });
    return (
      <div className={cardClasses}>
        <a>{ this.props.display }</a>
        <p>{ this.props.suit }</p>
      </div>
    );
  }
}

function initilizeSuit (suit, isRed) {
  for (let i = 1; i <= 13; i++) {
    const card = {
      id: 0,
      card: true,
      hidden: false,
      compress: false,
      isRed: isRed,
      suit: suit,
      value: i,
      display: '' + i
    }
    switch (i) {
      case 1: card.display = 'A'; card.value2 = 11; break
      case 11: card.display = 'J'; card.value = 10; break
      case 12: card.display = 'Q'; card.value = 10; break
      case 13: card.display = 'K'; card.value = 10; break
    }
    deck.push(card);
    card.id = deck.length
  }
}
function initilizeDeck () {
  initilizeSuit('♠', false);
  initilizeSuit('♣', false);
  initilizeSuit('♥', true);
  initilizeSuit('♦', true);
  shuffle(deck);
  console.log(deck);
}
function shuffle (deck) {
  for (let i = deck.length - 1; i > 0; i--) {
    let j = Math.floor(Math.random() * (i + 1));
    let temp = deck[i];
    deck[i] = deck[j];
    deck[j] = temp;
  }
}
function getCards (cards, compress, hidden) {
  return cards.map((card) => {
    return (
      <Card
        card = {card.card}
        compress = {compress}
        hidden = {hidden}
        isRed = {card.isRed}
        display = {card.display}
        suit = {card.suit}
        key = {card.id}>
      </Card>
    );
  });
}

export default App;
